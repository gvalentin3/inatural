//
//  MyObject.m
//  SpeakUp2
//
//  Created by GV on 11/29/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//  This is the implementation for the object that launches the thread.
//  UPDATED Dec 4 2011: Translated our serial commands into the functions used by kegpad.
//  Most notably: Read became sleeperRead(). Everyhing else is essentuially the same.

#import "MyObject.h"
#import "Serial.h"
#import <Foundation/Foundation.h>
#define SERIAL_PORT "/dev/tty.iap"

@implementation MyObject
+(void)aMethod:(id)param{
    
    
    printf("Succesfully Entered thread");
    char somechar[1]; //was originally somechar[8] in Hae Won's code
    int fd = openPort(SERIAL_PORT,115200);
    sleeperRead(fd, &somechar[0], 1); 
    if(fd>-1)
	{
        while(1) // Do this forever or until someone presses CTRL+C
        {
            read(fd,&somechar[0],1);  // Read a character over serial!
            write(fd,&somechar[0],1);
            
        }
	}
    
}
@end

