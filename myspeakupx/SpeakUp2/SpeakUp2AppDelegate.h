//
//  SpeakUp2AppDelegate.h
//  SpeakUp2
//
//  Created by Adrienne Haynes on 7/18/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegateProtocol.h"//copied from view sharing code  
#import "ExampleAppDataObject.h"//copied from view sharing code 
#import "SixthViewController.h" //copied from view sharing code 

@class ExampleAppDataObject;//copied from view sharing code

@interface SpeakUp2AppDelegate : NSObject <UIApplicationDelegate , AppDelegateProtocol> //Added AppDelegate Protocol
{
    UIWindow *window;
    UITabBarController *rootController; 
    
    UINavigationController *navigationController;//copied from view sharing code //needed?
	ExampleAppDataObject* theAppDataObject; //copied from view sharing code 
	IBOutlet SixthViewController* theSixthViewController; //copied from view sharing code 
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *rootController;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;  //copied from view sharing code 

@property (nonatomic, retain) ExampleAppDataObject* theAppDataObject;                //copied from view sharing code 
@property (nonatomic, retain) IBOutlet SixthViewController* theSixthViewController; //copied from view sharing code 

@end
