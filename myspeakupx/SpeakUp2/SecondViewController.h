//
//  SecondViewController.h
//  SpeakUp1
//
//  Created by Adrienne Haynes on 6/17/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@interface SecondViewController : UIViewController <AVAudioPlayerDelegate>
{
     UIButton * YesButton;
     UIButton * NoButton;
     UIButton * OKButton;
     UIButton * HelloButton;
     UIButton * ByeButton;
     UIButton * ThanksButton;
     UIButton * StopButton;
     UIButton * GoButton;
     UIButton * WaitButton;
     UIButton * ReturnButton;
     IBOutlet UILabel * ViewResponse; 
   
}

@property (nonatomic, retain) IBOutlet UILabel * ViewResponse;
@property (nonatomic, retain) IBOutlet UIButton * YesButton;
@property (nonatomic, retain) IBOutlet UIButton * NoButton;
@property (nonatomic, retain) IBOutlet UIButton * OKButton;
@property (nonatomic, retain) IBOutlet UIButton * HelloButton;
@property (nonatomic, retain) IBOutlet UIButton * ByeButton;
@property (nonatomic, retain) IBOutlet UIButton * ThanksButton;
@property (nonatomic, retain) IBOutlet UIButton * StopButton;
@property (nonatomic, retain) IBOutlet UIButton * GoButton;
@property (nonatomic, retain) IBOutlet UIButton * WaitButton;
@property (nonatomic, retain) IBOutlet UIButton * ReturnButton;

- (IBAction) buttonPressed: (id) sender;
- (IBAction) ReturnPressed: (id) sender;

- (IBAction) SayYes;
- (IBAction) SayNo;
- (IBAction) SayBye;
- (IBAction) SayOk;
- (IBAction) SayHello;
- (IBAction) SayThanks;
- (IBAction) SayStop;
- (IBAction) SayGo;
- (IBAction) SayWait;

@end
