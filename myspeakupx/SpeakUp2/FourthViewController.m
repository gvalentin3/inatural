//
//  FourthViewController.m
//  SpeakUp1
//
//  Created by Adrienne Haynes on 6/17/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import "FourthViewController.h"
#import <AVFoundation/AVAudioPlayer.h>

@implementation FourthViewController
@synthesize PlayButton;
@synthesize ReturnButton;
@synthesize ViewResponse;
@synthesize ReadButton;
@synthesize SleepButton;
@synthesize EatButton;
@synthesize DrinkButton;
@synthesize HaveButton;
@synthesize StayButton;
@synthesize GoButton;
@synthesize TryButton;
@synthesize ViewResponseNew;
@synthesize EatButtonPressed;
@synthesize BreakfastButton;
@synthesize LunchButton;
@synthesize DinnerButton;
@synthesize HaveButtonPressed;
@synthesize TryButtonPressed;
@synthesize TryThatButton;
@synthesize TryThisButton;
@synthesize HaveThatButton;
@synthesize HaveThisButton;

- (IBAction) buttonPressed: (id) sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"I would like to %@", title]; //printed on label: "Button Title"
    ViewResponse.text = newText;
    [newText release];
    PlayButton.hidden = YES;
    ViewResponse.hidden = NO;
    ReturnButton.hidden = NO;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;
    ViewResponseNew.hidden = YES;
    TryThatButton.hidden = YES;
    TryThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    HaveThisButton.hidden = YES;
}

- (IBAction) ReturnPressed: (id) sender
{
    PlayButton.hidden = NO;
    ViewResponse.hidden = YES;  
    ReturnButton.hidden = YES;
    ReadButton.hidden = NO;
    SleepButton.hidden = NO;
    EatButton.hidden = NO;
    DrinkButton.hidden = NO;
    HaveButton.hidden = NO;
    StayButton.hidden = NO;
    GoButton.hidden = NO;
    TryButton.hidden = NO;
    ViewResponseNew.hidden = YES;
    
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;
    
    TryThatButton.hidden = YES;
    TryThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    HaveThisButton.hidden = YES;
}

- (IBAction) EatButtonPressed: (id) sender
{
    PlayButton.hidden = YES;
    ViewResponse.hidden = YES;
    ViewResponseNew.hidden = YES;
    ReturnButton.hidden = YES;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    
    BreakfastButton.hidden = NO;
    LunchButton.hidden = NO;
    DinnerButton.hidden = NO;
    
    TryThatButton.hidden = YES;
    TryThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    HaveThisButton.hidden = YES;
}

- (IBAction) FoodSelected:(id)sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"I would like to eat %@", title]; //printed on label: "Button Title"
    ViewResponseNew.text = newText;
    [newText release];
    
    PlayButton.hidden = YES;
    ViewResponse.hidden = YES;
    ViewResponseNew.hidden = NO;
    ReturnButton.hidden = NO;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;    
    
    TryThatButton.hidden = YES;
    TryThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    HaveThisButton.hidden = YES;
}

- (IBAction) HaveButtonPressed: (id) sender
{
    PlayButton.hidden = YES;
    ViewResponse.hidden = YES;
    ViewResponseNew.hidden = YES;
    ReturnButton.hidden = YES;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;
    
    HaveThisButton.hidden = NO;
    HaveThatButton.hidden = NO;
    TryThisButton.hidden = YES;
    TryThatButton.hidden = YES;
}

- (IBAction) HaveThisSelected:(id)sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"I would like to have %@", title]; //printed on label: "Button Title"
    ViewResponseNew.text = newText;
    [newText release];
    
    PlayButton.hidden = YES;
    ViewResponse.hidden = YES;
    ViewResponseNew.hidden = NO;
    ReturnButton.hidden = NO;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;    
    
    HaveThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    TryThisButton.hidden = YES;
    TryThatButton.hidden = YES;
}

- (IBAction) HaveThatSelected:(id)sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"I would like to have %@", title]; //printed on label: "Button Title"
    ViewResponseNew.text = newText;
    [newText release];
    
    PlayButton.hidden = YES;
    ViewResponse.hidden = YES;
    ViewResponseNew.hidden = NO;
    ReturnButton.hidden = NO;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;    
    
    HaveThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    TryThisButton.hidden = YES;
    TryThatButton.hidden = YES;
}

- (IBAction) TryButtonPressed: (id) sender
{
    PlayButton.hidden = YES;
    ViewResponse.hidden = YES;
    ViewResponseNew.hidden = YES;
    ReturnButton.hidden = YES;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;
    
    HaveThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    TryThisButton.hidden = NO;
    TryThatButton.hidden = NO;
}

- (IBAction) TryThisSelected:(id)sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"I would like to try %@", title]; //printed on label: "Button Title"
    ViewResponseNew.text = newText;
    [newText release];
    
    PlayButton.hidden = YES;
    ViewResponse.hidden = YES;
    ViewResponseNew.hidden = NO;
    ReturnButton.hidden = NO;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;    
    
    HaveThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    TryThisButton.hidden = YES;
    TryThatButton.hidden = YES;
}

- (IBAction) TryThatSelected:(id)sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"I would like to try %@", title]; //printed on label: "Button Title"
    ViewResponseNew.text = newText;
    [newText release];
    
    PlayButton.hidden = YES;
    ViewResponse.hidden = YES;
    ViewResponseNew.hidden = NO;
    ReturnButton.hidden = NO;
    ReadButton.hidden = YES;
    SleepButton.hidden = YES;
    EatButton.hidden = YES;
    DrinkButton.hidden = YES;
    HaveButton.hidden = YES;
    StayButton.hidden = YES;
    GoButton.hidden = YES;
    TryButton.hidden = YES;
    
    BreakfastButton.hidden = YES;
    LunchButton.hidden = YES;
    DinnerButton.hidden = YES;    
    
    HaveThisButton.hidden = YES;
    HaveThatButton.hidden = YES;
    TryThisButton.hidden = YES;
    TryThatButton.hidden = YES;
}

- (IBAction) HaveThat
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Have that" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) HaveThis
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Have this" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeToGo
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like to go" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeBreakfast
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like breakfast" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeDinner
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like dinner" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeLunch
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like lunch" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeToPlay
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like to play" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeToRead
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like to read" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeToSleep
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like to sleep" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeToStay
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like to stay" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) LikeToDrink
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Like to drink" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play]; 
}

- (IBAction) TryThis
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Try this" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) TryThat
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Try That" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (void)dealloc
{
    [super dealloc];
    [PlayButton release];
    [ReadButton release];
    [ReturnButton release];
    [ViewResponse release];
    [SleepButton release];
    [EatButton release];
    [DrinkButton release];
    [HaveButton release];
    [StayButton release];
    [GoButton release];
    [TryButton release];
    [EatButtonPressed release];
    [ViewResponseNew release];
    [BreakfastButton release];
    [LunchButton release];
    [DinnerButton release];
    [TryThisButton release];
    [TryThatButton release];
    [HaveThatButton release];
    [HaveThisButton release];
}

@end
