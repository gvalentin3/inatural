//
//  FourthViewController.h
//  SpeakUp1
//
//  Created by Adrienne Haynes on 6/17/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>

@interface FourthViewController : UIViewController <AVAudioPlayerDelegate> 
{
    IBOutlet UILabel * ViewResponse;
    UIButton * PlayButton;
    UIButton * ReturnButton;
    UIButton * ReadButton;
    UIButton * SleepButton;
    UIButton * EatButton;
    UIButton * DrinkButton;
    UIButton * HaveButton;
    UIButton * StayButton;
    UIButton * GoButton;
    UIButton * TryButton;
    
    UIButton * EatButtonPressed;
    UIButton * BreakfastButton;
    UIButton * LunchButton;
    UIButton * DinnerButton;
    
    UIButton * HaveButtonPressed;
    UIButton * TryButtonPressed;
    UIButton * TryThisButton;
    UIButton * TryThatButton;
    UIButton * HaveThisButton;
    UIButton * HaveThatButton;
    
}

@property (nonatomic, retain) IBOutlet UIButton * PlayButton;
@property (nonatomic, retain) IBOutlet UIButton * ReturnButton;
@property (nonatomic, retain) IBOutlet UILabel * ViewResponse;
@property (nonatomic, retain) IBOutlet UIButton * ReadButton;
@property (nonatomic, retain) IBOutlet UIButton * SleepButton;
@property (nonatomic, retain) IBOutlet UIButton * EatButton;
@property (nonatomic, retain) IBOutlet UIButton * DrinkButton;
@property (nonatomic, retain) IBOutlet UIButton * HaveButton;
@property (nonatomic, retain) IBOutlet UIButton * StayButton;
@property (nonatomic, retain) IBOutlet UIButton * GoButton;
@property (nonatomic, retain) IBOutlet UIButton * TryButton;

@property (nonatomic, retain) IBOutlet UIButton * EatButtonPressed;
@property (nonatomic, retain) IBOutlet UILabel * ViewResponseNew;
@property (nonatomic, retain) IBOutlet UIButton * BreakfastButton;
@property (nonatomic, retain) IBOutlet UIButton * LunchButton;
@property (nonatomic, retain) IBOutlet UIButton * DinnerButton;

@property (nonatomic, retain) IBOutlet UIButton * HaveButtonPressed;
@property (nonatomic, retain) IBOutlet UIButton * TryButtonPressed;
@property (nonatomic, retain) IBOutlet UIButton * TryThisButton;
@property (nonatomic, retain) IBOutlet UIButton * TryThatButton;
@property (nonatomic, retain) IBOutlet UIButton * HaveThisButton;
@property (nonatomic, retain) IBOutlet UIButton * HaveThatButton;

- (IBAction) buttonPressed: (id) sender;
- (IBAction) ReturnPressed: (id) sender;
- (IBAction) EatButtonPressed: (id) sender;
- (IBAction) FoodSelected: (id) sender;
- (IBAction) HaveButtonPressed: (id) sender;
- (IBAction) TryButtonPressed: (id) sender;
- (IBAction) HaveThisSelected: (id) sender;
- (IBAction) HaveThatSelected: (id) sender;
- (IBAction) TryThisSelected: (id) sender;
- (IBAction) TryThatSelected: (id) sender;

- (IBAction) HaveThat;
- (IBAction) LikeBreakfast;
- (IBAction) LikeLunch;
- (IBAction) LikeDinner;
- (IBAction) LikeToGo;
- (IBAction) LikeToPlay;
- (IBAction) LikeToRead;
- (IBAction) LikeToSleep;
- (IBAction) LikeToStay;
- (IBAction) TryThat;
- (IBAction) TryThis;
- (IBAction) HaveThis;
- (IBAction) LikeToDrink;

@end
