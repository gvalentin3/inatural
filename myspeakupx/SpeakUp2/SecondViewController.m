//
//  SecondViewController.m
//  SpeakUp1
//
//  Created by Adrienne Haynes on 6/17/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import "SecondViewController.h"
#import <AVFoundation/AVAudioPlayer.h>

@implementation SecondViewController
@synthesize ViewResponse;
@synthesize YesButton;
@synthesize NoButton;
@synthesize OKButton;
@synthesize HelloButton;
@synthesize ByeButton;
@synthesize ThanksButton;
@synthesize StopButton;
@synthesize GoButton;
@synthesize WaitButton;
@synthesize ReturnButton;


- (IBAction) buttonPressed: (id) sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"%@ ", title]; //printed on label: "Button Title"
    ViewResponse.text = newText;
    [newText release];
    YesButton.hidden = YES;
    NoButton.hidden = YES;
    OKButton.hidden = YES;
    HelloButton.hidden = YES;
    ByeButton.hidden = YES;
    ThanksButton.hidden = YES;
    StopButton.hidden = YES;
    GoButton.hidden = YES;
    WaitButton.hidden = YES;
    ReturnButton.hidden = NO;
    ViewResponse.hidden = NO;
}

- (IBAction) ReturnPressed: (id) sender
{
    ReturnButton.hidden = YES;
    YesButton.hidden = NO;
    NoButton.hidden = NO;
    OKButton.hidden = NO;
    HelloButton.hidden = NO;
    ByeButton.hidden = NO;
    ThanksButton.hidden = NO;
    StopButton.hidden = NO;
    GoButton.hidden = NO;
    WaitButton.hidden = NO;
    ViewResponse.hidden = YES;
}

- (IBAction) SayYes
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Yes" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) SayNo
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"No" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) SayBye
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Bye" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) SayOk
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Ok" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) SayHello
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Hello" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) SayThanks
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Thanks" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) SayStop
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Stop" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) SayGo
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Go" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) SayWait
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Wait" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (void)dealloc
{
    [super dealloc];
    [ViewResponse release];
    [YesButton release];
    [NoButton release];
    [OKButton release];
    [HelloButton release];
    [ByeButton release];
    [ThanksButton release];
    [StopButton release];
    [GoButton release];
    [WaitButton release];
}
@end
