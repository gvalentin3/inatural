//
//  main.m
//  SpeakUp2
//
//  Created by VIP Fall 2011

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MyObject.h"
//#import "serial.h" //Moved elsewhere since xcode does not want to recognize it :(


int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    //[NSThread detachNewThreadSelector:@selector(aMethod:) toTarget:[MyObject class] withObject:nil];
    //Launch thread that will handle serial communication before GUI starts.//GV
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}