//
//  MyObject.h
//  SpeakUp2
//
//  Created by GV on 11/29/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//  This is the interface for the object that launches the thread.

#import <Foundation/Foundation.h>


@interface MyObject : NSObject
+(void)aMethod:(id)param;
    

@end
