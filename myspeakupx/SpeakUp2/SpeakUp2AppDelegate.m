//
//  SpeakUp2AppDelegate.m
//  SpeakUp2
//
//  Created by Adrienne Haynes on 7/18/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//
// Update: Nov 28,2011 
// Added view sharing stuff //GV
#import "SpeakUp2AppDelegate.h"
#import "MyObject.h"

@implementation SpeakUp2AppDelegate


@synthesize window=_window;
@synthesize rootController; 

@synthesize navigationController;  //copied from view sharing code 
@synthesize theAppDataObject;      //copied from view sharing code 
@synthesize theSixthViewController;//copied from view sharing code 


-(void) pushSixthView;
{
	[navigationController pushViewController: theSixthViewController animated:TRUE];
	
}

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self.window makeKeyAndVisible];
    [self.window addSubview:rootController.view];
    return YES;
}


#pragma mark -
#pragma mark Memory management

- (id) init; //copied from view sharing code
{
    [NSThread detachNewThreadSelector:@selector(aMethod:) toTarget:[MyObject class] withObject:nil];
    //Launch thread that will handle serial communication before GUI starts.//GV
	self.theAppDataObject = [[ExampleAppDataObject alloc] init];//copied from view sharing code
	[theAppDataObject release];                                 //copied from view sharing code
	return [super init];                                        //copied from view sharing code
}


- (void)dealloc
{

    [rootController release];
    [_window release];
    
    [navigationController release];    //copied from view sharing code
	self.theAppDataObject = nil;       //copied from view sharing code
	self.theSixthViewController = nil;//copied from view sharing code
	
	[super dealloc];
}

@end
