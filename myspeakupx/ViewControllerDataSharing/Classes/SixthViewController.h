//
//  SixthViewController.h
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright 2010 WareTo. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SixthViewController : UIViewController 
{
	IBOutlet UITextField* theInputView;
	IBOutlet UISlider*	 theSlider;
}

- (IBAction) sliderChanged: (UISlider*) theSlider;

@end
