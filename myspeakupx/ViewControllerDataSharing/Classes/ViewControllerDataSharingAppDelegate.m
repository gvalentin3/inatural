//
//  ViewControllerDataSharingAppDelegate.m
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright WareTo 2010. All rights reserved.
//

#import "ViewControllerDataSharingAppDelegate.h"

//#import "RootViewController.h" //apparently not needed
//#import "AppDelegateProtocol.h"//moved to .h
//#import "ExampleAppDataObject.h"//moved to .h



@implementation ViewControllerDataSharingAppDelegate

@synthesize window;
@synthesize navigationController;
@synthesize theAppDataObject;
@synthesize theSixthViewController;


-(void) pushSixthView;
{
	[navigationController pushViewController: theSixthViewController animated:TRUE];
	
}

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    [window addSubview:navigationController.view];
    [window makeKeyAndVisible];

    return YES;
}


#pragma mark -
#pragma mark Memory management

- (id) init;
{
	self.theAppDataObject = [[ExampleAppDataObject alloc] init];
	[theAppDataObject release];
	return [super init];
}



- (void)dealloc 
{
	[navigationController release];
	[window release];
	self.theAppDataObject = nil;
	self.theSixthViewController = nil;
	
	[super dealloc];
}


@end

