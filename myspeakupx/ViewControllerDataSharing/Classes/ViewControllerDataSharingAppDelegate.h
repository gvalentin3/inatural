//
//  ViewControllerDataSharingAppDelegate.h
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright WareTo 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegateProtocol.h" //removed from .m since it was included here already
#import "ExampleAppDataObject.h"//moved from .m

#import "SixthViewController.h"//why is root view controller h file not needed as well?




@class ExampleAppDataObject;

@interface ViewControllerDataSharingAppDelegate : NSObject <UIApplicationDelegate, AppDelegateProtocol> 
{
    UIWindow *window;
    UINavigationController *navigationController;
	ExampleAppDataObject* theAppDataObject;
	IBOutlet SixthViewController* theSixthViewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@property (nonatomic, retain) ExampleAppDataObject* theAppDataObject;
@property (nonatomic, retain) IBOutlet SixthViewController* theSixthViewController;

@end

