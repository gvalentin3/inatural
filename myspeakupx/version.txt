{\rtf1\ansi\ansicpg1252\cocoartf1038\cocoasubrtf350
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
\margl1440\margr1440\vieww9000\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\ql\qnatural\pardirnatural

\f0\b\fs24 \cf0 Revision1
\b0  \
11/1/2011\
by: Giancarlo Valentin\
Added serial code into main file\
i.e. includes were changed to imports\
No errors\
\

\b Revision2
\b0  \
11/3/2011\
by: Giancarlo Valentin\
Added MyStateObject.h\
Added MyStateObject.m\
added line in main to send message to MyStateObject.m\
(have not instantiated a MyStateObject)\
\
Some errors found relating to declaration of my state variable.\
\

\b Revision3
\b0  \
11/4/2011\
by: Giancarlo Valentin\
Changed names\
Added MyStateClass.h\
Added MyStateClass.m\
corrected line in main to send message to MyStateObject.m\
instantiated a MyStateObject\
\
<> = definitive path\
""  =  relative path\
\
No errors\
\

\b Revision4
\b0 \
11/30/2011\
by: Giancarlo Valentin\
Added two views from viewcontrollersharing example (Hae Won's Link)\
Sharing is occurring via the speakUP app delegate\
Serial code was moved out of main\
Implemented basic thread (Based on Tutorial)\
Switched our serial code for yelp's\
\

\b Revision5\

\b0 12/4/2011\
by: Giancarlo Valentin\
Switch the thread launch to app delegate (like David had done)\
Finished translating our serial commands intro Yelps.\
Our old Read became sleeperRead().}