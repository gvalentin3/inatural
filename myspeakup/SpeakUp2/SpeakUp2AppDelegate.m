//
//  SpeakUp2AppDelegate.m
//  SpeakUp2
//
//  Created by Adrienne Haynes on 7/18/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import "SpeakUp2AppDelegate.h"

@implementation SpeakUp2AppDelegate


@synthesize window=_window;
@synthesize rootController; 


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self.window makeKeyAndVisible];
    [self.window addSubview:rootController.view];
    return YES;
}

- (void)dealloc
{
    [_window release];
    [super dealloc];
    [rootController release];
}

@end
