//
//  FifthViewController.m
//  SpeakUp2
//
//  Created by Adrienne Haynes on 7/22/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import "FifthViewController.h"
#import "MyStateClass.h"

@implementation FifthViewController
@synthesize textField=_textField;
@synthesize label=_label;
@synthesize userName=_userName;




/*
@synthesize Button1;
@synthesize ViewResponse;

- (IBAction) buttonPressed: (id) sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"%@ ", title]; //printed on label: "Button Title"
    ViewResponse.text = newText;
    [newText release];
 
}


- (void)dealloc
{
    [super dealloc];
    [Button1 release];
    [ViewResponse release];
    
}
*/
- (IBAction)changeGreeting:(id)sender {
    
    self.userName = self.textField.text;
    
    NSString *nameString = self.userName;
    if ([nameString length] == 0) {
        nameString = @"World";
    }
    NSString *greeting = [[NSString alloc] initWithFormat:@"Hello, %@!", nameString];
    self.label.text = greeting;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.textField) {
        [theTextField resignFirstResponder];
    }
    return YES;
}

- (void)dealloc {
    [textField release];
    [label release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTextField:nil];
    [self setLabel:nil];
    [super viewDidUnload];
}
@end
