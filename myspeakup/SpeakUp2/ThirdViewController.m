//
//  ThirdViewController.m
//  SpeakUp1
//
//  Created by Adrienne Haynes on 6/17/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import "ThirdViewController.h"
#import <AVFoundation/AVAudioPlayer.h>

@implementation ThirdViewController
@synthesize ViewResponse;
@synthesize HappyButton;
@synthesize SadButton;
@synthesize AngryButton;
@synthesize SurprisedButton;
@synthesize ConfusedButton;
@synthesize SickButton;
@synthesize ReturnButton;

- (IBAction) buttonPressed: (id) sender
{
    NSString *title = [sender titleForState: UIControlStateNormal];
    NSString *newText = [[NSString alloc]initWithFormat: @"I am %@", title]; //printed on label: "Button Title"
    ViewResponse.text = newText;
    [newText release];
    HappyButton.hidden = YES;
    SadButton.hidden = YES;
    AngryButton.hidden = YES;
    SurprisedButton.hidden = YES;
    ConfusedButton.hidden = YES;
    SickButton.hidden = YES;
    ViewResponse.hidden = NO;
    ReturnButton.hidden = NO;
}

- (IBAction) ReturnPressed: (id) sender;
{
    ReturnButton.hidden = YES;
    HappyButton.hidden = NO;
    SadButton.hidden = NO;
    AngryButton.hidden = NO;
    SurprisedButton.hidden = NO;
    ConfusedButton.hidden = NO;
    SickButton.hidden = NO;
    ViewResponse.hidden = YES;    
}

- (IBAction) Happy
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"I am happy" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) Sad
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"I am sad" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) Angry
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"I am angry" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) Surprised
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"I am surprised" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) Sick
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"I am sick" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (IBAction) Confused
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"I am confused" ofType:@"mp3"];
    AVAudioPlayer* theAudio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    theAudio.delegate = self;
    [theAudio play];
}

- (void)dealloc
{
    [super dealloc];
    [ViewResponse release];
    [HappyButton release];
    [SadButton release];
    [AngryButton release];
    [SurprisedButton release];
    [ConfusedButton release];
    [SickButton release];
    [ReturnButton release];
}

@end
