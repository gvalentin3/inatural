//
//  FifthViewController.h
//  SpeakUp2
//
//  Created by Adrienne Haynes on 7/22/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FifthViewController : UIViewController <UITextFieldDelegate>{
//    UIButton * Button1;    
//    IBOutlet UILabel * ViewResponse;
    UITextField *textField;
    UILabel *label;
    NSString *userName;
}
@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) IBOutlet UITextField *textField;
@property (nonatomic, copy) NSString *userName;
- (IBAction)changeGreeting:(id)sender;

//@property (nonatomic, retain) IBOutlet UIButton * Button1;
//@property (nonatomic, retain) IBOutlet UILabel * ViewResponse;

//- (IBAction) buttonPressed: (id) sender;
@end
