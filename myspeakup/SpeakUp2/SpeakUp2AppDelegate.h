//
//  SpeakUp2AppDelegate.h
//  SpeakUp2
//
//  Created by Adrienne Haynes on 7/18/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakUp2AppDelegate : NSObject <UIApplicationDelegate> 
{
    UIWindow *window;
    UITabBarController *rootController; 
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *rootController;

@end
