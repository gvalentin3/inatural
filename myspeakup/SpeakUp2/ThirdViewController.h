//
//  ThirdViewController.h
//  SpeakUp1
//
//  Created by Adrienne Haynes on 6/17/11.
//  Copyright 2011 Norfolk State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ThirdViewController : UIViewController <AVAudioPlayerDelegate>
{
    IBOutlet UILabel * ViewResponse; 
    UIButton * HappyButton;
    UIButton * SadButton;
    UIButton * AngryButton;
    UIButton * SurprisedButton;
    UIButton * ConfusedButton;
    UIButton * SickButton;
    UIButton * ReturnButton;
}

@property (nonatomic, retain) IBOutlet UILabel * ViewResponse;
@property (nonatomic, retain) IBOutlet UIButton * HappyButton;
@property (nonatomic, retain) IBOutlet UIButton * SadButton;
@property (nonatomic, retain) IBOutlet UIButton * AngryButton;
@property (nonatomic, retain) IBOutlet UIButton * SurprisedButton;
@property (nonatomic, retain) IBOutlet UIButton * ConfusedButton;
@property (nonatomic, retain) IBOutlet UIButton * SickButton;
@property (nonatomic, retain) IBOutlet UIButton * ReturnButton;

- (IBAction) buttonPressed: (id) sender;
- (IBAction) ReturnPressed: (id) sender;

- (IBAction) Happy;
- (IBAction) Sad;
- (IBAction) Angry;
- (IBAction) Sick;
- (IBAction) Surprised;
- (IBAction) Confused;


@end
