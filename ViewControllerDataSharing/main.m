//
//  main.m
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright WareTo 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExampleAppDataObject.h"

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
