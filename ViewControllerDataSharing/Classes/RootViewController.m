//
//  RootViewController.m
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright WareTo 2010. All rights reserved.
//

#import "RootViewController.h"
#import "ExampleAppDataObject.h"
#import "AppDelegateProtocol.h"


@implementation RootViewController

#pragma mark -
#pragma mark instance methods
const char mybaby[2] = {'z','z'};

- (ExampleAppDataObject*) theAppDataObject;
{
	id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
	ExampleAppDataObject* theDataObject;
	theDataObject = (ExampleAppDataObject*) theDelegate.theAppDataObject;
	return theDataObject;
}

- (IBAction) sliderChanged: (UISlider*) sender;
{
	UISlider* aSlider = (UISlider*) sender;
	ExampleAppDataObject* theDataObject = [self theAppDataObject];
	theDataObject.float1 = aSlider.value;
}
	

#pragma mark -
#pragma mark UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return TRUE;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	ExampleAppDataObject* theDataObject = [self theAppDataObject]; 
	theDataObject.string1 = theInputView.text;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad 
 {
	 self.title = @"Root view";
	 UIBarButtonItem* myRightButton = [[[UIBarButtonItem alloc] initWithTitle: @"Second view" 
																		style: UIBarButtonItemStylePlain
																	   target: [UIApplication sharedApplication].delegate
																	   action: @selector(pushSecondView)] autorelease];
	 
	[self.navigationItem setRightBarButtonItem: myRightButton animated: FALSE];

    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated
{
	ExampleAppDataObject* theDataObject = [self theAppDataObject];
    
    // This updates the shared variable
    NSNumber * myInt = [[NSNumber alloc] initWithInt:(int)theDataObject.char1];
    NSString * temp3 = myInt.stringValue;
	theDataObject.string1 = temp3;
	
    theInputView.text = theDataObject.string1;
	theSlider.value = theDataObject.int1;
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)dealloc 
{
    [super dealloc];
}


@end

