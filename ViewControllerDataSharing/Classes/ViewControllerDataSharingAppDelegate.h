//
//  ViewControllerDataSharingAppDelegate.h
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright WareTo 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegateProtocol.h"
#import "SecondViewController.h"


@class ExampleAppDataObject;

@interface ViewControllerDataSharingAppDelegate : NSObject <UIApplicationDelegate, AppDelegateProtocol> 
{
    UIWindow *window;
    UINavigationController *navigationController;
	ExampleAppDataObject* theAppDataObject;
	IBOutlet SecondViewController* theSecondViewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@property (nonatomic, retain) ExampleAppDataObject* theAppDataObject;
@property (nonatomic, retain) IBOutlet SecondViewController* theSecondViewController;
@end

