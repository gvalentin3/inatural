//
//  ExampleAppDataObject.m
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.

#import "ExampleAppDataObject.h"
#import "Serial.h"


@implementation ExampleAppDataObject
@synthesize string1;
@synthesize data1;
@synthesize float1;
@synthesize char1;
@synthesize int1;
static int int1;
extern NSLock* lock;
#define SERIAL_PORT "/dev/tty.iap"

#pragma mark -
#pragma mark -Memory management methods

+(void)aMethod:(ExampleAppDataObject*)param
{
    printf("Succesfully Entered thread");
    int i = 0;
    char somechar; //was originally somechar[8] in Hae Won's code
    int fd = openPort(SERIAL_PORT,9600);//,115200); 
    
    if(1)
	{
        while(1) // Do this forever or until someone presses CTRL+C
        {
            //sleeperRead(fd,&somechar,1);  // Read a character over serial!
            
            /*
            param.char1 = somechar;
            param.int1 = (int) somechar;
            */
            
            param.int1 = i*10;
            param.char1 = (int)i;
            if (i > 100)
                i = 0;
            i++;
            printf("%i \n", i);
        }
	}
//    else
//        param.int1 = 1000.0;
}

+(void)update {}

+(char*)setString
{
    return "thread";
}

- (void)dealloc 
{
	//Release any properties declared as retain or copy.
	self.string1 = nil;
	self.data1 = nil;
	[super dealloc];
}
@end
