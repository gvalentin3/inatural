//
//  ViewControllerDataSharingAppDelegate.m
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright WareTo 2010. All rights reserved.
//

#import "ViewControllerDataSharingAppDelegate.h"
#import "RootViewController.h"
#import "AppDelegateProtocol.h"
#import "ExampleAppDataObject.h"



@implementation ViewControllerDataSharingAppDelegate

@synthesize window;
@synthesize navigationController;
@synthesize theAppDataObject;
@synthesize theSecondViewController;


-(void) pushSecondView;
{
	[navigationController pushViewController: theSecondViewController animated:TRUE];
}

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    [window addSubview:navigationController.view];
    [window makeKeyAndVisible];

    return YES;
}


#pragma mark -
#pragma mark Memory management

- (id) init;
{
    [ExampleAppDataObject setString];
	self.theAppDataObject = [[ExampleAppDataObject alloc] init];
    self.theAppDataObject.char1 = 'a';
    self.theAppDataObject.int1 = (int) self.theAppDataObject.char1;
    printf("hello\n\r");
    [NSThread detachNewThreadSelector:@selector(aMethod:) toTarget:[ExampleAppDataObject class] withObject:self.theAppDataObject];
	[theAppDataObject release];
    
	return [super init];
}



- (void)dealloc 
{
	[navigationController release];
	[window release];
	self.theAppDataObject = nil;
	self.theSecondViewController = nil;
	
	[super dealloc];
}


@end

