//
//  SecondViewController.m
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright 2010 WareTo. All rights reserved.
//

#import "SecondViewController.h"
#import "ExampleAppDataObject.h"
#import "AppDelegateProtocol.h"


@implementation SecondViewController

#pragma mark -
#pragma mark instance methods

- (ExampleAppDataObject*) theAppDataObject;
{
	id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
	ExampleAppDataObject* theDataObject;
	theDataObject = (ExampleAppDataObject*) theDelegate.theAppDataObject;
	return theDataObject;
}

- (IBAction) sliderChanged: (id) sender;
{
	NSInteger value;
	UISlider* aSlider = (UISlider*) sender; 
	ExampleAppDataObject* theDataObject = [self theAppDataObject];
	value = aSlider.value;
	theDataObject.float1 = value;
    printf("%c",theDataObject.char1);
}

#pragma mark -
#pragma mark UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return TRUE;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
	ExampleAppDataObject* theDataObject = [self theAppDataObject]; 
	theDataObject.string1 = theInputView.text;
}

#pragma mark -
#pragma mark View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	self.title = @"Second view";
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
	ExampleAppDataObject* theDataObject = [self theAppDataObject];
    
    // This updates the shared variable
    NSNumber * myInt = [[NSNumber alloc] initWithInt:(int)theDataObject.char1];
    NSString * temp3 = myInt.stringValue;
	theDataObject.string1 = temp3;
    
	theInputView.text = theDataObject.string1;
	theSlider.value = theDataObject.int1;
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc 
{
    [super dealloc];
}


@end
