//
//  RootViewController.h
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.
//  Copyright WareTo 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController 
{
	IBOutlet UITextField* theInputView;
	IBOutlet UISlider*	 theSlider;
}

- (IBAction) sliderChanged: (UISlider*) theSlider;

@end
