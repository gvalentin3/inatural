//
//  ExampleAppDataObject.h
//  ViewControllerDataSharing
//
//  Created by Duncan Champney on 7/29/10.

#import <Foundation/Foundation.h>
#import "AppDataObject.h"
#import "Serial.h"


@interface ExampleAppDataObject : AppDataObject 
{
	NSString*	string1;
	NSData*		data1;
    int	int1;
	CGFloat		float1;
    char char1;
}

+(void)aMethod:(ExampleAppDataObject*)param;
+(char*)setString;

@property (nonatomic, copy) NSString* string1;
@property (nonatomic, retain) NSData* data1;
@property (nonatomic) CGFloat	float1;
@property (nonatomic) NSInteger int1;
@property (nonatomic) char char1;

@end
